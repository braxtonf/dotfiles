# Up function
# Goes up n directorues
# Usage: up 4 - goes up 4 directories
up() {
    COUNTER=$1
    while [[ $COUNTER -gt 0 ]]
        do
            UP="${UP}../"
            COUNTER=$(( $COUNTER -1 ))
        done
    if [ "$2" = "-l" ]; then
        cdl $UP
    else
        cd $UP
    fi
    UP=''
}

# CD into a directory and list all of its contents
# Usage: cdl {DIR}
cdl() {
    cd "$@"
    ls -a
}

# Collatz Conjecture in bash code
# Usage: collatz {number} OR colz {number}
collatz() {
    if [ "$1" ]; then
        N=$1
    else
        N=$RANDOM
    fi

    ORIG=$N

    echo "Starting number: "$N
    echo $N

    STEP=0

    while [ "$N" -ne "1" ]
        do
            if [ "$(($N % 2))" -eq "0" ]; then
                N=$(($N / 2))
            else
                N=$((($N * 3) + 1))
            fi
            echo $N
            STEP=$(($STEP + 1))
        done

    echo "The number $ORIG took $STEP steps."
}

# Backup files
# Usage: backup (filename[s])
backup() {
    if [ ! -f "$1" ]; then
        echo "You must input a file as the first argument!"
        exit 1
    fi

    for f in "$@"
        do
            cp -a "$f" "$f".$(date +%Y%m%d%H%M)
        done
}

# Copy progress meter
# Usage: cp_p (copy from) (copy to)
cp_p() {
    rsync -WavP --human-readable --progress $1 $2
}

# Make a directory and cd into it
# Usage
mkcd() {
    mkdir "$@"
    cd "$1"
}
