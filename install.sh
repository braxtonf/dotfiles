HOME="/home/$USER"
RESOURCE="$HOME/.bf"
CODE="$HOME/.bf/dotfiles"
BACKUP="$HOME/.bf/user/backups"
WAITTIME="2"

GREEN="\033[0;32m"
RED="\033[0;31m"
RESET="\033[0m"
CYAN="\033[36m"
BCYAN="\033[1;36m"
YELLOW="\033[1;33m"

echo " $CYAN╔═══════════════════════════════════╗"
echo " ║  "$BCYAN"Braxton's Dotfiles Installation$RESET$CYAN  ║"
echo " ╠═══════════════════════════════════╣"
echo " ║     "$BCYAN"Link https://git.io/vwGBs$RESET$CYAN     ║"
echo " ╚═══════════════════════════════════╝$RESET"

if [ -d "$CODE" ]; then
    echo
    echo "$RED [X] Previous installation found in ~/.bf/dotfiles. Remove and try again."
    exit 1
else
    echo
    echo "$GREEN [✓] No previous installation"
fi

sleep $WAITTIME

command -v git >/dev/null 2>&1 || {
    echo
    echo "$YELLOW [!] I require git but it's not installed";
    echo "Git is not installed. Install now?[Y/n]: "
    read git
    case $git in
        [yY] | [yY][eE][sS] )
            sudo apt-get -qq install git
            echo
            echo "$GREEN [✓] Install Git"
            ;;
        [nN] | [nN][oO] )
            echo
            echo "$RED [X] Git is required for continuation"
            exit 1
            ;;
    esac
}

echo
echo "$GREEN [✓] Git is installed"
sleep $WAITTIME

if [ ! -d "$RESOURCE" ]; then
    mkdir $RESOURCE
    echo
    echo "$GREEN [✓] Create resource directory"
else
    echo
    echo "$GREEN [✓] Resource directory located"
fi

sleep $WAITTIME

git clone --quiet https://github.com/braxtonfair/dotfiles $CODE
echo
echo "$GREEN [✓] Clone repository to ~/.bf/dotfiles"

if [ ! -d "$BACKUP" ]; then
    mkdir -p $BACKUP
    echo
    echo "$GREEN [✓] Create backup directory"
else
    echo
    echo "$YELLOW [!] There is already a backup directory! Override? [Y/n]";
    read overr
    case $overr in
        [yY] | [yY][eE][sS] )
            rm -rf $BACKUP
            echo
            echo "$GREEN [✓] Remove backup directory"
            ;;
        [nN] | [nN][oO] )
            echo
            echo "$RED [X] Please remove the current backup directory in ~/.bf/backups before continuing"
            exit 1
            ;;
    esac
fi
sleep $WAITTIME

mv ~/.bash* $BACKUP
echo
echo "$GREEN [✓] Back up files"
sleep $WAITTIME
cp $CODE/.bashrc ~/
echo
echo "$GREEN [✓] Install Braxton's configuration"

command -v atom >/dev/null 2>&1 || {
    echo
    echo "$YELLOW [!] I highly recommend the Atom text editor by Github.";
    echo "Do you want to install it now? [Y/n]: "
    read atom
    case $atom in
        [yY] | [yY][eE][sS] )
            sudo add-apt-repository ppa:webupd8team/atom
            sudo apt-get update
            sudo apt-get install atom
            echo
            echo "$GREEN [✓] Install Atom"
            ;;
        [nN] | [nN][oO] )
            break;;
    esac
}

echo
echo ""$CYAN" Congratulations! You have successfully installed the configuration. Please source the new .bashrc! (source ~/.bashrc)"
