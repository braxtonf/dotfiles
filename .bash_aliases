# File system aliases
alias mkdir="mkdir -pv"
alias wget="wget -c"
alias det="cd ~/Desktop"
alias docs="cd ~/Documents"
alias pict="cd ~/Pictures"
alias mus="cd ~/Music"
alias dld="cd ~/Downloads"
alias vid="cd ~/Videos"
alias sl="ls"
alias brc="$EDITOR ~/.bashrc && source ~/.bashrc"
alias bal="$EDITOR ~/.bf/dotfiles/.bash_aliases && source ~/.bashrc"
alias bfu="$EDITOR ~/.bf/dotfiles/.bash_functions && source ~/.bashrc"
alias back="cd $OLDPWD"
alias fhere="find . -name"
alias hs="history | grep"
alias as="alias | grep"

# System aliases
alias c="clear"
alias rlb=". ~/.bashrc"
alias uninstall-bf="sh ~/.bf/dotfiles/uninstall.sh"

# Git commands
alias g="git"
alias gc="git clone"
alias gp="git push && git pull"
alias gcm="git commit -m"
alias gi="git init"
alias ga="git add"
alias gaa="git add --all"
alias gm="git mv"
alias gre="git reset"
alias grm="git rm"
alias gbi="git bisect"
alias gl="git log"
alias gs="git status"
alias gsh="git show"
alias gb="git branch"
alias gd="git diff"
alias gm="git merge"
alias gch="git checkout"

# Python aliases

# PHP aliases
alias laravel="~/.composer/vendor/laravel/installer/laravel"

# ETC/Fun commands
alias colz="collatz"
