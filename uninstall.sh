HOME="/home/$USER"
RESOURCE="$HOME/.bf"
CODE="$HOME/.bf/dotfiles"
BACKUP="$HOME/.bf/user/backups"
WAITTIME="2"

GREEN="\033[0;32m"
RED="\033[0;31m"
RESET="\033[0m"
CYAN="\033[36m"
BCYAN="\033[1;36m"
YELLOW="\033[1;33m"

BACKUPHERE="0"

echo " $CYAN╔═════════════════════════════════════╗"
echo " ║  "$BCYAN"Braxton's Dotfiles Uninstallation$RESET$CYAN  ║"
echo " ╠═════════════════════════════════════╣"
echo " ║      "$BCYAN"Link https://git.io/vwGBs$RESET$CYAN      ║"
echo " ╚═════════════════════════════════════╝$RESET"

sleep $WAITTIME

if [ -d "$CODE" ]; then
    echo
    echo "$GREEN [✓] Previous installation found in ~/.bf/dotfiles"
else
    echo
    echo "$RED [X] No previous installation"
    exit 1
fi

sleep $WAITTIME

if [ -d "$BACKUP" ]; then
    echo
    echo "$GREEN [✓] Backups found"
    BACKUPHERE="1"
else
    echo
    echo "$YELLOW [!] No backups found.. will copy from /etc/skel/.bashrc"
    BACKUPHERE="0"
fi

sleep $WAITTIME

if [ $BACKUPHERE -eq "0" ]; then
    # No backups
    rm -rf $CODE
    echo
    echo "$GREEN [✓] Remove existing code repository"

    cp "/etc/skel/.bashrc" $HOME
    sleep $WAITTIME

    echo
    echo "$GREEN [✓] Reverted"

    echo
    echo $CYAN"Thank you for trying my dotfile configuration! I hope you liked it, see you soon? $GREEN:)$RESET"

    exit 0
fi

# Backups are present

rm -rf $CODE
echo
echo "$GREEN [✓] Remove existing code repository"

sleep $WAITTIME

rm $HOME/.bashrc
echo
echo "$GREEN [✓] Removed configured .bashrc"

sleep $WAITTIME

cp $BACKUP/.bash* ~/
echo
echo "$GREEN [✓] Retrieved old user files"

sleep $WAITTIME

rm -rf $RESOURCE
echo
echo "$GREEN [✓] Cleanup left over files"

echo
echo $CYAN"Thank you for trying my dotfile configuration! I hope you liked it, see you soon? $GREEN:)$RESET"
