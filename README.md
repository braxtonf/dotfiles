# [Braxton's](https://github.com/braxtonfair) dotfiles
This is my configuration for Ubuntu. I have these files changed:

* .bashrc - Base configuration, sources the aliases and functions
* .bash_aliases - All of the aliases
* .bash_functions - All of the functions

I only have the .bashrc in the root directory, and I clone the code repo into ~/.bf/dotfiles and source the alias and function files from there.

# Installation
### Automatically:
Simply run this command in your terminal:

    wget -qO- https://raw.githubusercontent.com/braxtonfair/dotfiles/master/install.sh | sh

This will create a directory named ~/.bf. This is where all the source code of the repository will be located and where your current data backup will be.

### Manually:
To install my configuration, just copy the files specified above into your **root** directory *usually ~/*.

**WARNING:** If you do not plan on keeping these settings, *please* make a backup of your original files. I will not be held responsible for any misuse, or accidental data loss while you use this configuration.

# Uninstallation
### Automatically:
Simply run this command in your terminal:

    uninstall-bf

This will delete the local git repository of this, then removed the .bashrc that I tampered with, then retrieve any files from ~/.bf/user/backups/.

### Manually:

If you want to uninstall manually, just remove the ~/.bashrc file and copy all of the contents from the ~/.bf/user/backups folder into the root(~/) directory.
